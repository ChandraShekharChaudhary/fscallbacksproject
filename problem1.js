let fs = require("fs");

function creatFilesInDir(number, location, callBack) {
  //create a directory in the given location.
  fs.mkdir(`${location}jsonDir`, (err) => {
    if (err) {
      throw err;
    } else {
      for (let num = 1; num <= number; num++) {
        fs.writeFile(`${location}jsonDir/file${num}.json`, "data", (err) => {
          if (err) {
            throw err;
          }
          console.log(`Created file${num} in jsonDir directory `);
        });
      }
      console.log("Created jsonDir directory");
      callBack(number, location);
    }
  });
}

//remove all the file that has beed created in directory.
function deleteFileAndDir(number, location) {
  for (let num = 1; num <= number; num++) {
    fs.rm(`${location}jsonDir/file${num}.json`, (err) => {
      if (err) {
        throw err;
      } else {
        console.log(`Deleted file${num} from jsonDir directory`);
      }
    });
  }
  //delete jsonDir directory.
  fs.rm(`${location}jsonDir`, { recursive: true, force: true }, (err) => {
    if (err) {
      throw err;
    } else {
      console.log("Deleted jsonDir directory");
    }
  });
}

module.exports.createFileInDir = creatFilesInDir;
module.exports.deleteFileAndDir = deleteFileAndDir;
