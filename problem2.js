let fs = require("fs");

//helper function.
function append(location, fileName) {
  fs.appendFile(`${location}filenames.txt`, `${fileName}\n`, (err) => {
    if (err) {
      throw err;
    } else {
      console.log(`${fileName} file name added in the filenames.txt`);
    }
  });
}

//1.
function readFile(location, callback) {
  fs.readFile(`${location}lipsum.txt`, "utf8", (err, data) => {
    if (err) {
      throw err;
    } else {
      callback(data);
    }
  });
}

//2.
function upperCase(location, data, callback) {
  fs.writeFile(`${location}upperCase.txt`, data.toUpperCase(), (err) => {
    if (err) {
      throw err;
    } else {
      console.log("upperCase text file created");
      // append(location, "filenames.txt");
      append(location, "upperCase.txt");
      callback();
    }
  });
}

//3.
function lowerCase(location, callBack) {
  fs.readFile(`${location}upperCase.txt`, "utf8", (err, data) => {
    if (err) {
      return err;
    }
    fs.writeFile(
      `${location}lowerCase.txt`,
      data.toString().toLowerCase().replace(/\./g, ".\n"),
      (err) => {
        if (err) {
          throw err;
        } else {
          console.log(
            "data of uppercase converted in lover case and then split in sentences then put it in the file name lowerCase.txt"
          );
          append(location, "lowerCase.txt");
          callBack();
        }
      }
    );
  });
}

//4.
function sortContents(location, callback) {
  fs.readFile(`${location}lowerCase.txt`, "utf8", (err, data) => {
    if (err) {
      return err;
    }
    fs.writeFile(
      `${location}lowerCaseSort.txt`,
      data.split("\n").sort().join("\n"),
      (err) => {
        if (err) {
          throw err;
        } else {
          console.log("lowerCaseSort file created");
          append(location, "lowerCaseSort.txt");
          callback();
        }
      }
    );
  });
  fs.readFile(`${location}upperCase.txt`, "utf8", (err, data) => {
    if (err) {
      return err;
    }
    fs.writeFile(
      `${location}upperCaseSort.txt`,
      data.split("\n").sort().join("\n"),
      (err) => {
        if (err) {
          throw err;
        } else {
          console.log("upperCaseSort file created");
          append(location, "upperCaseSort.txt");
          callback();
        }
      }
    );
  });
}

//5
function deleteFiles(location, callBack) {
  fs.readFile(`${location}filenames.txt`, "utf8", (err, data) => {
    data
      .split("\n")
      .filter((element) => element !== "")
      .map((element) => {
        fs.rm(`${location}${element}`, (err) => {
          if (err) {
            throw err;
          } else {
            console.log(`file ${element} deleted`);
          }
        });
      });
    callBack();
  });
}

module.exports.readFile = readFile;
module.exports.upperCase = upperCase;
module.exports.lowerCase = lowerCase;
module.exports.sortContents = sortContents;
module.exports.deleteFiles = deleteFiles;
