let problem2 = require('../problem2.js');
let path=require("path");
let location =path.join(__dirname,'./');

problem2.readFile(location, (data) => {
  problem2.upperCase(location, data, () => {
    problem2.lowerCase(location, () => {
      problem2.sortContents(location, () => {
        problem2.deleteFiles(location, () => {
          console.log("work done");
        });
      });
    });
  });
});
