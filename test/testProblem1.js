let problem1 = require("../problem1.js");
/*createFilesThenDelete function takes two arguments
1. number of files that to be create.
2. Location where to be directory create.
*/
let path=require("path");
let location=path.join(__dirname,'./');

problem1.createFileInDir(10, location, (a, b) => {
  problem1.deleteFileAndDir(a, b);
});
